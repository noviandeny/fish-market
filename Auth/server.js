const express = require('express')
const app = express()
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const bcrypt = require('bcrypt')
const User = require('./model/user')
const jwt = require('jsonwebtoken')
const fs = require('fs')
const connectionString = `mongodb://localhost:27017/fish-market`
const privateKey = fs.readFileSync('./private.pem', 'utf-8')
const auth = require('./middleware/jwt_auth')

const options = {
    autoIndex: false,
    reconnectTries: Number.MAX_VALUE,
    reconnectInterval: 500,
    poolSize: 10,
    bufferMaxEntries: 0,
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
}

mongoose.connect(connectionString, options, err => {
    if (err) console.log(err)
    else console.log("Connected to MongoDB!")
})

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// TODO
/**
 * 1. validate email
 * 2. validate phone
 * 3. validate password (should have minimum|maximum length)
 */
app.post('/auth/register', (req, res) => {
    if (req.body.name != undefined 
        && req.body.username != undefined 
        && req.body.password != undefined
        && req.body.email != undefined
        && req.body.address != undefined
        && req.body.phone != undefined) {
        User.findOne({ username: req.body.username })
            .then(data => {
                if (data == undefined) {
                    req.body.password = bcrypt.hashSync(req.body.password, 10)
                    delete req.body.role
                    return User({ ...req.body }).save()
                }
                else return Promise.reject(new Error("username already taken!"))
            })
            .then(() => {
                res.send({ status: "success", token: jwt.sign({ username: req.body.username, role: "user" }, privateKey, { algorithm: 'RS256' }) })
            })
            .catch(err => {
                res.status(400).send({ status: "failed", message: err.toString() })
            })
    }
    else {
        res.status(400).send({ status: "failed", message: "pliss fill all slurr :((((" })
    }
})

app.post('/auth/login', (req, res) => {
    if (req.body.username != undefined && req.body.password != undefined) {
        User.findOne({ username: req.body.username })
            .then(data => {
                if (data == undefined) return Promise.reject(new Error("username or password wrong!"))
                else if (bcrypt.compareSync(req.body.password, data.password)) return Promise.resolve(data)
                else return Promise.reject(new Error("username or password wrong!"))
            })
            .then(data => {
                res.send({ status: "success", token: jwt.sign({ username: data.username, role: data.role }, privateKey, { algorithm: 'RS256' }) })
            })
            .catch(err => {
                res.status(401).send({ status: "failed", message: err.toString() })
            })
    }
    else {
        res.status(401).send({ status: "failed", message: "pliss fill all slurr :((((" })
    }
})

app.get('/user', auth.user, (req,res) => {
    let payload = jwt.decode(req.headers.token)
    if (payload.username != undefined){
        User.findOne({username: payload.username})
            .then(data => {
                res.send({status:"success", message:data})
            })
            .catch(err => {
                res.status(400).send({status:"failed",message:err.toString()})
            })
    }
})

app.listen(process.env.PORT, process.env.HOST, () => console.log(`Auth app listening on http://${process.env.HOST}:${process.env.PORT}!`))


function escapeRegExp(string) {
    if (string) return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched stringe
    else return ""
}
