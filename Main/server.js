const express = require('express')
const app = express()
const request = require('request')
const cors = require('cors')

app.use(cors())

app.use(['/auth','/user'], (req, res) => {
    var url = 'http://0.0.0.0:8083' + req.originalUrl
    req.pipe(request(url)).pipe(res)
})

app.use('/store', (req, res) => {
    var url = 'http://0.0.0.0:8084' + req.originalUrl
    req.pipe(request(url)).pipe(res)
})

app.use('/product', (req, res) => {
    var url = 'http://0.0.0.0:8085' + req.originalUrl
    req.pipe(request(url)).pipe(res)
})

app.use('/transaction', (req, res) => {
    var url = 'http://0.0.0.0:8086' + req.originalUrl
    req.pipe(request(url)).pipe(res)
})

app.listen(process.env.PORT, process.env.HOST, () => console.log(`Main app listening on http://${process.env.HOST}:${process.env.PORT}!`))
