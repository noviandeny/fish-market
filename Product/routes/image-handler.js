const express = require('express');
const fileUpload = require('express-fileupload');
const jwt = require('jsonwebtoken');
const bodyParser = require('body-parser');
const auth = require('../middleware/jwt_auth');
const imageDIR = __dirname + '/../files/images/';
const fs = require('fs');

const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(fileUpload());

//USER GET image using url
app.get('/product/image/:store/:name',auth.user, function (req, res) {
    let store = req.query.store;
    let name = req.query.name;
    let imageURL = store + '.' + name +'.jpg';
    if (store !== undefined && name !== undefined){
        try{
            if (fs.existsSync(imageDIR+imageURL))
                res.sendFile(imageDIR + imageURL);
            else res.status(404).send({status:'Not Found', message: 'File moved or deleted'});
        }
        catch (err) {
            console.log(err);
        }
    }
});

//store add image
app.post('/store/product/image',auth.user , function (req,res) {
    let token = jwt.decode(req.header.token);
    let store = ''//belum ada
    let name = req.body.name;
    let imageName = req.files.image.name;
    let imageURL = store + '.' + name +'.jpg';
    if (!req.files || Object.keys(req.files).length === 0) {
        res.status(400).send('No files were uploaded.');
    }

    else if(!imageName.search('.jpg'))
        res.status(400).send('file type not allowed');

    else if (name !==undefined && store !== undefined) {
        let image = req.files.image;
        image.mv(imageDIR + imageURL,(err) => {
            if(err)
                res.status(500).send({status: 'failed', message: err.toString()});
        });
    }
});

module.exports = {
    app:app,
    deleteImage: function(store, name, res){
        let imageURL = store + '.' + name +'.jpg';
        try{
            if(fs.existsSync(imageDIR + imageURL))
                fs.unlink(imageDIR+imageURL, (err) => {
                    if(err)
                        res.status(500).send({status: 'failed', message: err.toString()});
                });
            else
                res.status(404).send({status:'Not Found', message: 'File moved or deleted'});
        }
        catch (err) {
            console.log(err);
        }
    }
}