const express = require('express');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const bodyParser = require('body-parser');
const mongodb = `mongodb://localhost:27017/fish-market`;
const auth = require('../middleware/jwt_auth');
const Product = require('../model/product');
const Store = require('../model/store');
const imageHandler = require('./image-handler');

const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const options = {
    autoIndex: false,
    reconnectTries: Number.MAX_VALUE,
    reconnectInterval: 500,
    poolSize: 10,
    bufferMaxEntries: 0,
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
};

mongoose.connect(mongodb, options, err => {
    if (err) console.log(err);
    else console.log("Connected to MongoDB!");
});

//product page
app.get('/product', function (req, res) {
    Product.find({})
        .limit(20)
        .then(data => res.send({status:"success", message:data}))
        .catch(err => {
            res.status(400).send({ status: "failed", message: err.toString() });
        });
});

//product search
app.get('/product/search', function (req, res) {
    /*TO DOO
    use order
     */
    let store = req.query.store;
    let name = req.query.name;
    let order = req.query.order;

    if(order === undefined && store === undefined && name === undefined){
        Product.find({})
            .limit(20)
            .then(data => res.send({status:"success",message:data}))
            .catch(err => {
                res.status(400).send({ status: "failed", message: err.toString() });
            });
    }

    else if(store !== undefined && name !== undefined){
        Product.find({store: store, name: { $regex : '.*' + name + '.*'}})
            .limit(20)
            .then(data => res.send({status:"success",message:data}))
            .catch(err => {
                res.status(400).send({ status: "failed", message: err.toString() });
            });
    }

    else if(name !== undefined){
        Product.find({name: { $regex : '.*' + name + '.*'}})
            .limit(20)
            .then(data => res.send({status:"success",message:data}))
            .catch(err => {
                res.status(400).send({ status: "failed", message: err.toString() });
            });
    }

    else if(store !== undefined){
        Product.find({store: store})
            .limit(20)
            .then(data => res.send({status:"success",message:data}))
            .catch(err => {
                res.status(400).send({ status: "failed", message: err.toString() });
            });
    }

    else res.status(400).send({ status: "failed"});
});

//seller create a product
app.post('/product/create', auth.user, function (req, res) {
    let token = jwt.decode(req.headers.token);
    let store = req.body.store;
    let name = req.body.name;
    let price = req.body.price;
    let stock = (req.body.stock === undefined) ? 0:req.body.stock;
    let description = req.body.description;

    let query = {
        name: name,
        description: description,
        store: store,
        stock: stock,
        price: price
    };

    if (name !== undefined && store !== undefined && price !== undefined){
        Store.findOne({ _id: store, ownerUsername: token.username })
            .then( data => {
                if(data != undefined){
                    Product.findOne({name: name, store: store})
                        .then(data => {
                            if(data != null) return Promise.reject(new Error("product name duplicate!"))
                            return Product({...query}).save() 
                        })
                        .then(data => {
                            return res.send({status:"success", message:data})
                        })
                        .catch(err => {
                            res.status(400).send({status:"failed", message:err.toString()})
                        })
                }
                else{
                    res.status(400).send({status: "failed",message:"store not found"});
                }
            })
    }
    else res.status(400).send({status: "failed",message:"fill all field"});
});

//seller update a product
app.put('/product/update', auth.user, function (req, res) {
    let token = jwt.decode(req.headers.token);
    let store = req.body.store;
    let name = req.body.name;
    let price = req.body.price;
    let stock = (req.body.stock === undefined) ? 0:req.body.stock;
    let description = req.body.description;

    let query = {
        name: name,
        description: description,
        store: store,
        stock: stock,
        price: price
    };
    if (name !== undefined && store !== undefined && price !== undefined){
        Store.findOne({ _id: store, ownerUsername: token.username})
            .then(data => {
                if (data != undefined) {
                    Product.findOneAndUpdate({store: store, name: name}, query)
                        .then(() => {
                            res.send({ status: "success", message: "succesfully update a product"})
                        })
                        .catch(err => {
                            res.status(400).send({ status: "failed", message: err.toString() });
                        });
                }
                else {
                    res.status(400).send({ status: "failed", message: "unable to find product" });
                }
            })  
    }
    else res.status(400).send({status: "failed",message:"fill all field"});
});

//seller delete a product
app.delete('/product/delete', auth.user, function (req, res) {
    let store = req.body.store;
    let name = req.body.name;
    let token = jwt.decode(req.headers.token);
    if (name !== undefined && store !== undefined){
        Store.findOne({ _id: store, ownerUsername: token.username})
            .then(data => {
                if (data != undefined) {
                    Product.findOneAndDelete({store: store, name: name})
                        .then(data => {
                            if (data != undefined) res.send({ status: "success", message: "succesfully delete a product"})
                            else res.send({ status: "failed", message: "product not found"})
                        })
                        .catch(err => {
                            res.status(400).send({ status: "failed", message: err.toString() });
                        });
                }
                else {
                    res.status(400).send({ status: "failed", message: "unable to find product" });
                }
            })
    }
    else res.status(400).send({status: "failed", message:"fill all field"});
});

module.exports = app;