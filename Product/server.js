const express = require('express');
const productHandler = require('./routes/product-handler');
const imageHandler = require('./routes/image-handler');
const app = express();

app.use(productHandler);
app.use(imageHandler.app);

app.listen(process.env.PORT, process.env.HOST, () => console.log(`Product app listening on http://${process.env.HOST}:${process.env.PORT}!`));
