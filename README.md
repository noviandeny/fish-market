
# Fish Market

Ini adalah project membuat fish market. Project ini sangat ribet, maka dari itu donasi dari Anda sangat dibutuhkan.

# [](#role)Role

Kelompok B

-   Soumindar: bikin mockup + logo
-   Ganjar, Novian, Okka: API Product (get product, edit product, dsb), API User, API Toko (buat toko dsb)
-   Alam, Novian, Okka: API penjualan (beli), API supplier, API fintech
-   Wattic: Mobile App

# [](#how-to)How To

1.  Bikin folder masing-masing sesuai kebutuhan
2.  Kuding aja, yang penting jadi

# [](#how-to-run)How To Run

1.  Run command `npm install` di folder utama (folder terluar), lalu juga di setiap sub folder project (Auth, Main, Product, Store, Transaction)
2.  Dari folder terluar, jalankan `npm run dev`
3.  Aplikasi utama dapat diakses pada localhost port 8080

# Contributor
Manusia yang bekerja secara terpaksa
<table>
<tr>
<td align="center"><img src="https://i.ibb.co/F4N5zK0/alam.png" width="100px;" alt="Alamsyah Imanudin"/><br /><sub><b>Alam</b></sub></a><br /></td>
<td align="center"><img src="https://i.ibb.co/hgJzWkq/circle-cropped.png" width="100px;" alt="Ganjar Muhammad Parikesit"/><br /><sub><b>Ganjar</b></sub></a><br /></td>
<td align="center"><img src="https://i.ibb.co/qs0VJGb/novian.png" width="100px;" alt="Novian Deny Cahyoaji"/><br /><sub><b>Novian</b></sub></a><br /></td>
<td align="center"><img src="https://i.ibb.co/m4kP4dG/okka.png" width="100px;" alt="M Taufan Okka Muhiba"/><br /><sub><b>Okka</b></sub></a><br /></td>
<td align="center"><img src="https://i.ibb.co/DfsTWDR/qolby.png" width="100px;" alt="Soumindar Qolby"/><br /><sub><b>Qolby</b></sub></a><br /></td>
<td align="center"><img src="https://i.ibb.co/0fRSzZ8/waffiq.png" width="100px;" alt="Waffiq Maaroja"/><br /><sub><b>Waffiq</b></sub></a><br /></td>
</tr>
</table>

# Supervisor
"Faster, faster, c'mon"
<table>
<td align="center"><img src="https://i.ibb.co/h8Y9Hrb/azhari.png" width="200px;" alt="Azhari Satria Nugraha"/><br /><sub><b>Azhari SN</b></sub></a><br /></td>
</table>


