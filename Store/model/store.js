const mongoose = require('mongoose')

var storeSchema = new mongoose.Schema({
    name: { type: String, require: true},
    ownerUsername: { type: String, require: true},
})
var Stores = mongoose.model('Stores', storeSchema)

module.exports = Stores