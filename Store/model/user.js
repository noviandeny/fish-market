const mongoose = require('mongoose')

var userSchema = new mongoose.Schema({
    name: { type: String, required: true },
    username: { type: String, required: true },
    password: { type: String, required: true },
    email: { type: String, required: true },
    address: { type: String, required: true },
    phone: { type: String, required: true },
    store: [ String ],
    role: { type: String, default: 'user' },
    money: { type: Number, default: 0 }
})
var Users = mongoose.model('Users', userSchema)

module.exports = Users