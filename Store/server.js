const express = require('express')
const app = express()
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const Store = require('./model/store')
const User = require('./model/user')
const jwt = require('jsonwebtoken')
const connectionString = `mongodb://localhost:27017/fish-market`
const auth = require('./middleware/jwt_auth')

const options = {
    autoIndex: false,
    reconnectTries: Number.MAX_VALUE,
    reconnectInterval: 500,
    poolSize: 10,
    bufferMaxEntries: 0,
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
}

mongoose.connect(connectionString, options, err => {
    if (err) console.log(err)
    else console.log("Connected to MongoDB!")
})

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.get('/store', (req,res) => {
    Store.find({})
        .then( data => res.send({status:"success", message:data}))
        .catch(err => {
            res.status(400).send({ status: "failed", message: err.toString() })
        })
})

app.post('/store/create', auth.user, (req,res) => {
    let payload = jwt.decode(req.headers.token)
    let query = { name: req.body.name, ownerUsername: payload.username}
    if (req.body.name != undefined){
        Store.findOne({ name: payload.name })
            .then( data => {
                if (data == undefined) {
                    return Store({...query}).save()
                }
                else return Promise.reject(new Error("username already taken!"))
            })
            .then(async data => {
                await User.findOne({username:payload.username})
                .then(user => {
                    let newStores = user.store.concat(data._id)
                    return newStores
                })
                .then(async newStores => {
                    await User.findOneAndUpdate({username:payload.username}, {store:newStores})
                })
                res.send({status:"success", message:data})
            })
            .catch(err => {
                res.status(400).send({ status: "failed", message: err.toString() })
            })
    }
    else {
        res.status(400).send({ status: "failed", message: "pliss fill all slurr :((((" })
    }
})



app.listen(process.env.PORT, process.env.HOST, () => console.log(`Store app listening on http://${process.env.HOST}:${process.env.PORT}!`))