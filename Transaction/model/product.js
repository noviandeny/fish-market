const mongoose = require('mongoose');

let productSchema = new mongoose.Schema({
    name : { type: String, require: true },
    description : { type: String},
    store : { type: String, require: true },
    stock : {
        type: Number,
        require: true,
        validate: {
            validator: Number.isInteger,
            message: '{VALUE} is not an integer value'
        }
    },
    price : { type: Number, require: true }
});

let Product = mongoose.model('Product', productSchema);
module.exports = Product;