const express = require('express')
const app = express()
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const Store = require('./model/store')
const User = require('./model/user')
const Product = require('./model/product')
const jwt = require('jsonwebtoken')
const connectionString = `mongodb://localhost:27017/fish-market`
const auth = require('./middleware/jwt_auth')

const options = {
    autoIndex: false,
    reconnectTries: Number.MAX_VALUE,
    reconnectInterval: 500,
    poolSize: 10,
    bufferMaxEntries: 0,
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
}

mongoose.connect(connectionString, options, err => {
    if (err) console.log(err)
    else console.log("Connected to MongoDB!")
})

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// buy
app.post('/transaction/buy', auth.user, (req,res) => {
    let payload = jwt.decode(req.headers.token)
    let query = { productID: req.body.productID, qty: req.body.qty, buyer: payload.username }
    if (query.productID != undefined && query.qty != undefined && query.productID.length==query.qty.length){
        let ids = []
        let qtys = []
        let stores = []
        let seller = []
        for (var i=0;i<query.productID.length;i++){
            ids.push(query.productID[i])
            qtys.push(query.qty[i])
        }
        Product.find({
            '_id': { $in: ids}})
            .then( async datas => {
                let totalPrice = 0
                if(datas != undefined && datas.length == ids.length){
                    for(var i=0;i<ids.length;i++){
                        if(datas[i].stock<qtys[i]) {
                            return Promise.reject(new Error("eror qty"))
                        }
                        stores.push(datas[i].store)
                        await Product.findOneAndUpdate({_id:datas[i]._id},{stock: datas[i].stock-qtys[i]})
                        totalPrice += qtys[i] * datas[i].price
                    }
                    query.totalPrice=totalPrice
                    return datas
                }
                else return Promise.reject(new Error("gak ketemu barangnya"))
            })
            .then( async (datas) => {
                await Store.find({'_id': { $in: stores }})
                    .then(store_list => {
                        for(var i=0;i<store_list.length;i++){
                            seller.push(store_list[i].ownerUsername)
                        }
                    })
                    return datas
            })
            .then(async (datas)=>{
                for(var i=0;i<datas.length;i++){
                    await User.findOne({username:seller[i]})
                    .then(async sl => {
                        await User.findOneAndUpdate({username:seller[i]},{money:sl.money + (qtys[i]*datas[i].price)})
                    })
                }
            })
            .then(async ()=>{
                await User.findOne({username:query.buyer})
                    .then(async (us) => {
                        await User.findOneAndUpdate({username:query.buyer},{money:us.money-query.totalPrice})
                    })
            })
            .then(()=>{
                res.send({status:"success", message:"Berhasil beli ikan"})
            })
            .catch(err => {
                return res.status(400).send({status:"failed", message:err.toString()})
            })
    }
    else {
        res.status(400).send({ status: "failed", message: "pliss fill all slurr :((((" })
    }
})

// top up
app.post('/transaction/topup', auth.user, (req,res) => {
    let payload = jwt.decode(req.headers.token)
    let query = { amount: req.body.amount}
    if (query.amount != undefined){
        User.findOne({username:payload.username})
            .then(data=>{
                let nextMoney = data.money + query.amount
                User.findOneAndUpdate({username:payload.username}, {money:nextMoney}, (err,result)=>{
                    if(err){
                        return res.status(400).send({status:"failed", message:err.toString()})
                    }
                    res.send({status:"success", message:"berhasil nambah duit"})
                })
                
            })
            .catch(err=>{
                res.status(400).send({ status: "failed", message: err.toString() })
            })
    }
    else {
        res.status(400).send({ status: "failed", message: "pliss fill all slurr :((((" })
    }
})

app.listen(process.env.PORT, process.env.HOST, () => console.log(`Transaction app listening on http://${process.env.HOST}:${process.env.PORT}!`))